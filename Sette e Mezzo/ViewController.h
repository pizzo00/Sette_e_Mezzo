//
//  ViewController.h
//  Sette e mezzo
//
//  Created by DavideMac on 09/11/16.
//  Copyright © 2016 Davide Pizzolato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *cartaButton;
@property (weak, nonatomic) IBOutlet UIButton *stopButton;
@property (weak, nonatomic) IBOutlet UIButton *rigiocaButton;
@property (weak, nonatomic) IBOutlet UILabel *puntiBanco;
@property (weak, nonatomic) IBOutlet UILabel *puntiGiocatore;
@property (weak, nonatomic) IBOutlet UIImageView *winLose;
@property (weak, nonatomic) IBOutlet UILabel *vinte;
@property (weak, nonatomic) IBOutlet UILabel *perse;

- (IBAction)carta:(id)sender;
- (IBAction)stop:(id)sender;
- (IBAction)rigioca:(id)sender;


@end

